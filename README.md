# skywar-deobfuscation

Les actionscript sont extrait avec flare dans les fichier .flr. 
Il est possible de récupérer les assets contenu dans les fichier swf avec ffdec.

* decode.txt Les variable obfusqué avec dans certains cas leur valeur
* Island.as Le code actionscript pour crée une île à partir de sa seed
* map.as Le code actionscript pour crée la carte à partir de l'id de la partie
* script.rb un script pour déobfuquer les symboles que j'ai pu traduire
* traceIsland.as Le code actionscript pour dessiner une île

Ici on essaye de deobfuscer les fichiers du jeu


Dans client.flr
4361 draw planet on map
6770 Animation des combats
10282 Création de la carte
11875 Définition de World
13579 Probablement dessin de l'ile
